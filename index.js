'use strict';

import Autocomplete from './lib/Autocomplete';
import Admob from './lib/Admob.js';
import Button from './lib/Button';
import CodeScanner from './lib/CodeScanner';
//import DataExport from './lib/DataExport';
//import DataImport from './lib/DataImport';
import DataList from './lib/DataList';
import EbosModal from './lib/Modal';
import EbosSwitch from './lib/Switch';
import InputCurrency from './lib/InputCurrency';
import InputSelect from './lib/InputSelect';
import InputDate from './lib/InputDate';
import InputText from './lib/InputText';
import Label from './lib/Label';
import MenuExport from './lib/MenuExport';
import MenuImport from './lib/MenuImport';
import ModalDropdown from './lib/ModalDropdown';
import OutputText from './lib/OutputText';
import OutputLink from './lib/OutputLink';
import PanelGrid from './lib/PanelGrid';
import SelectPanel from './lib/SelectPanel';

module.exports = {
  Autocomplete,
  Admob,
  Button,
  //DataExport,
  //DataImport,
  DataList,
  CodeScanner,
  Switch: EbosSwitch,
  InputCurrency,
  InputSelect,
  InputDate,
  InputText,
  Label,
  MenuExport,
  MenuImport,
  ModalDropdown,
  Modal: EbosModal,
  OutputText,
  OutputLink,
  PanelGrid,
  SelectPanel
};