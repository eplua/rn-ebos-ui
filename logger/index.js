'use strict';
import Config from 'react-native-config';

const enable = Config.ENVIRONMENT == 'development';

const logger = (msg, method, alias) => {
    const date = new Date();
    let message = ''; 
    try{
        message = msg ? msg.reduce((accumulator, currentValue) => accumulator + ' ' + JSON.stringify(currentValue)) : '';
    } catch {
        message = msg ? msg.reduce((accumulator, currentValue) => accumulator + ' ' + currentValue) : '';
    }
    console[method](`${alias ? alias : method.toUpperCase()} [${date.toLocaleDateString()} ${date.toLocaleTimeString()}]: ` + message );
};

const Log = {
    info: (...msg) => {
        logger(msg, 'info');
    },
    error: (...msg) => {
        logger(msg, 'warn', 'ERROR');
    },
    warn: (...msg) => {
        logger(msg, 'info', 'WARN');
    },
    debug: (...msg) => {
        if(enable)
            logger(msg, 'debug');
    },
    trace: (...msg) => {
        if(enable)
            logger(msg, 'debug', 'TRACE');
    }
};

export default Log;