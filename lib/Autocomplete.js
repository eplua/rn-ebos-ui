'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ListView,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  ViewPropTypes,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

class Autocomplete extends Component {
  static propTypes = {
    ...TextInput.propTypes,

    value: PropTypes.func,
    /**
     * These styles will be applied to the container which
     * surrounds the autocomplete component.
     */
    containerStyle: ViewPropTypes.style,
    /**
     * Assign an array of data objects which should be
     * rendered in respect to the entered text.
     */
    //data: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
    data: PropTypes.func,
    /*
     * These styles will be applied to the container which surrounds
     * the textInput component.
     */
    inputContainerStyle: ViewPropTypes.style,
    /*
     * These styles will be applied to the container which surrounds
     * the result list.
     */
    listContainerStyle: ViewPropTypes.style,
    /**
     * These style will be applied to the result list.
     */
    listStyle: ViewPropTypes.style,
    /**
     * `onShowResults` will be called when list is going to
     * show/hide results.
     */
    onShowResults: PropTypes.func,
    /**
     * `renderItem` will be called to render the data objects
     * which will be displayed in the result view below the
     * text input.
     */
    renderItem: PropTypes.func,
    /**
     * `renderSeparator` will be called to render the list separators
     * which will be displayed between the list elements in the result view
     * below the text input.
     */
    renderSeparator: PropTypes.func,
    /**
     * renders custom TextInput. All props passed to this function.
     */
    renderTextInput: PropTypes.func
  };

  static defaultProps = {
    data: [],
    renderItem: rowData => <Text>{rowData}</Text>,
    renderSeparator: null,
    renderTextInput: props => <TextInput {...props} />,
    showBorders: true,
    minChars: 3,
    autoCapitalize: 'none',
    autoCorrect: false,
    required: false,
    editable: true,
    flexLabel: 0.5
  };

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
        dataSource: ds,
        query: this.props.value()
    };
  }

  componentWillReceiveProps(props){
      const query = props.value();
      if(query){
          this.setState({
              query
          });
      }
  }

  loadDataList(query){
    let dataList = query.length >= this.props.minChars ? this.props.data(query) : [];
    if(dataList.length == 0 && this.props.onEmpty){
        this.props.onEmpty();
    }
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(dataList),
      query
    });
  }

  /**
   * Proxy `blur()` to autocomplete's text input.
   */
  blur() {
    const { textInput } = this;
    textInput && textInput.blur();
  }

  /**
   * Proxy `focus()` to autocomplete's text input.
   */
  focus() {
    const { textInput } = this;
    textInput && textInput.focus();
  }

  renderResultList() {
    const { dataSource } = this.state;
    const { listStyle, renderItem, renderSeparator, onSelection } = this.props;

    return (
      <ListView
        dataSource={dataSource}
        keyboardShouldPersistTaps="always"
        renderRow={rowData => {
            return (<TouchableOpacity onPress={() => {
                            onSelection(rowData);
                            this.setState({
                                dataSource: this.state.dataSource.cloneWithRows([]),
                                query: this.props.value()
                            });
                        }}
                    >
                    {renderItem(rowData)}
                    </TouchableOpacity>);
        }}
        renderSeparator={renderSeparator}
        style={[styles.list, listStyle]}
      />
    );
  }

  renderTextInput() {
    const { onEndEditing, renderTextInput, style } = this.props;
    const readOnlyStyle = this.props.editable ? {} : styles.readOnlyStyle;
    const propsParent = {...this.props};
    delete propsParent.value;
    const props = {
      style: [styles.input, style, readOnlyStyle],
      ref: ref => (this.textInput = ref),
      onEndEditing: e => onEndEditing && onEndEditing(e),
      onChangeText: text => { this.loadDataList(text); },
      value: this.state.query,
      ...propsParent
    };
    return renderTextInput(props);
  }

  defaultRender(){
      const { dataSource, query } = this.state;
      const {
        containerStyle,
        inputContainerStyle,
        listContainerStyle,
        onShowResults
      } = this.props;
      const showResults = dataSource.getRowCount() > 0;
      const borderStyle = this.props.showBorders ? border : {};

      // Notify listener if the suggestion will be shown.
      onShowResults && onShowResults(showResults);

      return <View style={[styles.container, containerStyle]}>
        <View style={[borderStyle, styles.inputContainer, inputContainerStyle,{flexDirection:'row', flex:1}]}>
            {this.renderTextInput()}
            {this.props.editable && (query && query.length) > 0 &&
                <TouchableOpacity onPress={()=>{
                    this.loadDataList('');
                    this.focus();
                }}>
                    <Text style={{right:3, top:13 }}><Icon name="close" size={14}/></Text>
                </TouchableOpacity>
            }
            {this.props.editable && this.props.renderButtons}
        </View>
        { showResults &&
            <View style={[styles.listContainerStyle, listContainerStyle]}>
              {this.props.renderHeader()}
              {this.renderResultList()}
            </View>
        }
      </View>
  }

  render() {
    const readOnlyStyle = this.props.editable ? {} : {borderBottomWidth:0.5, borderBottomColor: '#c8c7cc', marginTop: 1, marginBottom:1};
    const bottonHeight = this.props.editable ? 1 : 0;
    return (
        this.props.label ?
         (<View style={[{flexDirection: 'row'}, readOnlyStyle]}>
             <View style={{flexDirection: 'row', flex:this.props.flexLabel, backgroundColor:'white',borderBottomWidth:this.props.editable?0.5:0, marginTop: bottonHeight, marginBottom: bottonHeight, borderBottomColor: '#c8c7cc', paddingTop:6}}>
                 <Text style={styles.label}>{this.props.label}</Text>
                 <Text style={styles.required}>{this.props.required && this.props.editable && '*'}</Text>
             </View>
             <View style={{flex: 1, height:this.props.editable?35:32, paddingLeft:8}}>
                 {this.defaultRender()}
            </View>
          </View>
         ) : (
             this.defaultRender()
         )
    );
  }
}

const border = {
  borderColor: '#b9b9b9',
  borderRadius: 1,
  borderWidth: 1
};

const androidStyles = {
  container: {
    zIndex: 1,
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    backgroundColor: 'white'
  },
  inputContainer: {
//    ...border,
    marginBottom: 0
  },
  list: {
    ...border,
    backgroundColor: 'white',
    borderTopWidth: 0,
    marginTop: 0
  }
};

const iosStyles = {
  container: {
    ...androidStyles.container
  },
  inputContainer: {
    ...border
  },
  input: {
    backgroundColor: 'white',
    height: 40,
    paddingLeft: 3
  },
  list: {
    ...border,
    backgroundColor: 'white',
    borderTopWidth: 0,
    left: 0,
    position: 'absolute',
    right: 0
  }
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    paddingLeft: 3,
    flex:1
  },
  readOnlyStyle: {
      color: '#808080'
  },
  label: {
      textAlignVertical: 'center',
      color: 'black',
      padding: 2,
      backgroundColor:'white'
  },
  listContainerStyle: {
      borderWidth: 0.75,
      borderColor: '#809080'
  },
  required: {
      color:'red',
      textAlignVertical: 'center',
      paddingBottom: 4
  },
  ...Platform.select({
    android: { ...androidStyles },
    ios: { ...iosStyles }
  })
});

export default Autocomplete;
