'use strict'

import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity} from 'react-native';

class Button extends Component {

    constructor(props) {
        super(props);
        const backgroundColor = props.color in styles ? styles[props.color] : {'backgroundColor': props.color};

        this.style = [
            styles.button,
            {
                backgroundColor: props.style ? this.props.style.backgroundColor || 'transparent' : backgroundColor
            },
            {
                width: props.width || (props.style ? props.style.width || 'auto' : 'auto')
            }, 
            {                
                height: props.height
            }
        ];
    }

    static defaultProps = {
      color: 'transparent',
      height:40,
      disabled: false
    };

    UNSAFE_componentWillReceiveProps(nextProps){
        if(this.props.color != nextProps.color){
            this.style = [...this.style, styles[nextProps.color]]; 
        }
    }

    render() {
        let disabled = this.props.disabled ? styles.disabled : {};

        return <TouchableOpacity onPress={this.props.onPress} disabled={this.props.disabled} 
            style={[{flexDirection:'row', alignItems:'center', justifyContent:'center', paddingHorizontal: 5}, this.props.containerStyle, this.style]}>
                {this.props.iconSrc &&  <Image style={styles.icon} source={iconSrc} />}
                <Text style={[{color: 'white', fontSize: this.props.style ? this.props.style.fontSize || 14 :  14}, disabled]}>
                    {this.props.value || this.props.children}
                </Text>
                {this.props.label && <Text style={this.props.labelStyle}>{' ' + this.props.label}</Text>}            
        </TouchableOpacity>
    }

}

const styles = StyleSheet.create({
    button: {
        textAlign: 'center',
        textAlignVertical: 'center',
        color: 'white',
        borderRadius: 4,
        margin: 4
    },
    disabled: {
        opacity: 0.6
    },
    green: {
        backgroundColor: '#4CAF50'
    },
    blue: {
        backgroundColor: '#008CBA'
    },
    red: {
        backgroundColor: '#f44336'
    },
    gray: {
        backgroundColor: '#e7e7e7',
        color: 'black'
    },
    black: {
        backgroundColor: '#555555'
    },
    transparent: {
        backgroundColor: 'transparent'
    },
    icon: {
        width: 28,
        height: 28,
        marginLeft: 10,
        marginRight: 30
      }

})

export default Button;
