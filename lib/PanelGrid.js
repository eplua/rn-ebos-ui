'use strict'

import React, {Component} from 'react';
import {View} from 'react-native';

class PanelGrid extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        columns: 2,
        columnsStyle: []
    };

    render() {
        let columnArray = [], rowarray = [];
        let i = 1, lastIndex = this.props.children.length -1;
        for(let index in this.props.children){
            let item = this.props.children[index];
            rowarray.push(
                <View key={index} style={[{flex:1}, this.props.columnsStyle[i-1]]}>
                    {item}
                </View>
            );
            if(index == lastIndex && rowarray.length < this.props.columns){
                for (let j = rowarray.length; j < this.props.columns; j++) {
                    rowarray.push(
                        <View key={index+j} style={{flex:1,borderBottomColor: '#c8c7cc',
                        borderBottomWidth: 0.5, marginTop:1, marginBottom:1}}></View>
                    );
                }
            }

            if(i == this.props.columns || index == lastIndex){
                columnArray.push(
                    <View key={index+':'+i} style={[{flexDirection: 'row',alignItems:'center',borderBottomColor:'#c8c7cc'}, this.props.style]}>
                        {rowarray}
                    </View>
                );
                rowarray = [];
                i = 1;
            } else {
                i++;
            }
        }
        return <View style={[{flexDirection: 'column'}, this.props.containerStyle]}>
            {columnArray}
        </View>
    }

}

export default PanelGrid;
