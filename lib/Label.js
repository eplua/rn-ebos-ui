'use strict'

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

class Label extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        editable: true,
        required: false
    }

    render() {
        return <View style={styles.container}>
                    <Text style={styles.label}>{this.props.value}</Text>
                    <Text style={styles.required}>{this.props.required && this.props.editable && '*'}</Text>
                </View>
    }

}

const types = {
    text: 'default',
    decimal: 'numeric'
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      borderBottomColor: '#c8c7cc',
      borderBottomWidth: 0.5,
      flexDirection: 'row',
      marginTop:1,
      marginBottom:1
    },
    label: {
      padding: 2,
      color: 'black',
      textAlignVertical:'center'
    },
    required: {
        color:'red',
        textAlignVertical: 'center',
        paddingBottom: 4
    }
})

export default Label;
