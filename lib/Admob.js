'use strict'

import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import { BannerAd, BannerAdSize } from '@react-native-admob/admob';

export default class Admob extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {

    }

    render() {
        return <View style={styles.container}>
          {/* <AdMobBanner
            adSize="smartBannerPortrait"
            adUnitID={Config.ADMOBPROID_BANNER}
            testDevices={[AdMobBanner.simulatorId]}
            onAdFailedToLoad={error => console.log(error)}
          /> */}
            <BannerAd
                size={BannerAdSize.BANNER}
                unitId={this.props.admobproid}
                onAdFailedToLoad={(error) => console.error(error)}
            />
        </View>
    }

}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    }
})