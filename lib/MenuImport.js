'use strict'

import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Menu, {MenuOptions, MenuOption, MenuTrigger} from 'react-native-popup-menu';
import Icon from 'react-native-vector-icons/MaterialIcons';

class MenuImport extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        editable: true,
        required: false,
        size: 22
    }

    render() {
        return <Menu style={{marginTop: 7, marginRight: 10}} onSelect={this.props.onSelect}>
            <MenuTrigger>
                <Text><Icon name="file-upload" size={this.props.size} color='#4F8EF7'/></Text>
            </MenuTrigger>
            <MenuOptions optionsContainerStyle={{width: 160}}>
                <MenuOption value={0}>
                    <Text style={styles.menuOption}>Importar productos</Text>
                </MenuOption>
                <MenuOption value={1}>
                    <Text style={styles.menuOption}>Descargar plantilla</Text>
                </MenuOption>
            </MenuOptions>
        </Menu>
    }

}

const styles = StyleSheet.create({
    menuOption: {
        fontSize:15
    }
})

export default MenuImport;
