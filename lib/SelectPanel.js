'use strict'

import React, { Component } from 'react';
import { Modal, StyleSheet, Text, TouchableHighlight, TouchableOpacity, ScrollView, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
const dropDownIcon = (<Icon name="arrow-drop-down" size={20} color='gray' />)

class SelectPanel extends Component {

  static defaultProps = {
    type: 'text',
    onChangeValue: () => { },
    height: 35,
    placeholder: 'Select...',
    textStyle: { fontSize: 14, color: 'black' },
    objectKey: 'id',
    editable: true,
    required: false
  }

  constructor(props) {
    super(props);
    let objSelection = {};
    objSelection[this.props.objectKey] = -1,
      objSelection['toString'] = () => {
        return this.props.placeholder;
      };
    this.state = {
      objSelection,
      currentOrientation: 'unknown',
      transparent: true,
      selectPanelVisible: false
    };
  }

  focus() {
  }

  toggle() {
    this._setModalVisible(!this.state.selectPanelVisible);
  }

  _setModalVisible(visible) {
    this.setState({ selectPanelVisible: visible });
  }

  _renderRow(rowData, rowID, highlighted) {
    let evenRow = rowID % 2;
    return (
      <TouchableHighlight underlayColor='cornflowerblue'>
        <View style={[styles.row]}>
          <Text style={[styles.row_text, highlighted && { color: 'rgb(255, 64, 129)' }]}>
            {rowData.toString()}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    var modalBackgroundStyle = {
      backgroundColor: this.state.transparent ? 'rgba(0, 0, 0, 0.5)' : '#f5fcff'
    };
    var innerContainerTransparentStyle = this.state.transparent
      ? { backgroundColor: '#fff', padding: 20 }
      : null;

    const readOnlyStyle = this.props.editable ? { color: 'black' } : styles.readOnlyStyle;
    const placeholderText = this.props.editable ? this.props.placeholder : '';
    let data = Object.values(this.props.data);
    let selected = this.props.value;
    let index = -1;
    if (selected) {
      if (!this.props.required) {
        data.unshift(this.state.objSelection);
      }
      index = data.findIndex((elem) => {
        return elem[this.props.objectKey] == selected[this.props.objectKey]
      });
    }

    return <View style={styles.container}>
      {this.props.label &&
        <View style={[{ flexDirection: 'row', flex: 0.5 }, this.props.labelStyle]}>
          <Text style={styles.label}>{this.props.label}</Text>
          <Text style={styles.required}>{this.props.required && this.props.editable && '*'}</Text>
        </View>
      }
      <View style={{ flexDirection: 'row', flex: 1 }}>
        {this.props.editable ? (
          <TouchableOpacity onPress={this.toggle.bind(this)} style={{ flexDirection: 'row', flex: 1 }} >
            <Text style={[styles.input, readOnlyStyle, { flex: 1 }]}>{selected ? (selected.toString ? selected.toString() : placeholderText) : placeholderText}</Text>
            <Text>{dropDownIcon}</Text>
          </TouchableOpacity>) : (
            <Text style={[styles.input, { flex: 1 }]}>{selected ? (selected.toString ? selected.toString() : placeholderText) : placeholderText}</Text>
          )}
      </View>
      <Modal
        animationType='none'
        transparent={this.state.transparent}
        visible={this.state.selectPanelVisible}
        onRequestClose={this.toggle.bind(this)}
        supportedOrientations={['portrait', 'landscape']}
        onOrientationChange={evt => this.setState({ currentOrientation: evt.nativeEvent.orientation })}
      >
        <View style={[styles.modalContainer, modalBackgroundStyle]}>
          <View style={[styles.innerContainer, innerContainerTransparentStyle]} >
            <ScrollView keyboardShouldPersistTaps='always'>
              {this.props.children}
            </ScrollView>
          </View>
        </View>
      </Modal>
    </View>
  }

}

const types = {
  text: 'default',
  decimal: 'numeric'
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderBottomColor: '#c8c7cc',
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    marginTop: 1,
    marginBottom: 1
  },
  label: {
    padding: 2,
    color: 'black',
    textAlignVertical: 'center'
  },
  input: {
    flex: 1,
    padding: 2,
    marginRight: -3,
    paddingRight: 0
  },
  row: {
    flexDirection: 'row',
    height: 40,
    alignItems: 'center'

  },
  row_text: {
    marginHorizontal: 10,
    fontSize: 16,
    color: 'rgba(0, 0, 0, 0.87)',
    textAlignVertical: 'center',
  },
  readOnlyStyle: {
    color: '#808080'
  },
  required: {
    color: 'red',
    textAlignVertical: 'center',
    paddingBottom: 4
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  innerContainer: {
    borderRadius: 10
  }
})

export default SelectPanel;
