'use strict'

import React, {Component} from 'react';
import {Alert, Modal, StyleSheet, Text, View} from 'react-native';
import Button from './Button';

import ModalPicker from 'react-native-modal-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
const publishIcon = (<Icon name="publish" size={22} />)

import RNFetchBlob from 'rn-fetch-blob';
const { readFile, dirs:{ DocumentDir } } = RNFetchBlob.fs;

//* xlsx.js (C) 2013-present  SheetJS -- http://sheetjs.com
import XLSX from 'xlsx';


class DataImport extends Component {

    static defaultProps = {
    }

    constructor(props) {
        super(props);
        this.state = {
            transparent: true,
            modalFilterVisible: false,
            currentOrientation: 'unknown',
            modalExportVisible: false,
            dataFiles: [],
            dataDirs: [],
            importFile: ''
        };
    }

    componentDidMount() {
        this.loadDataList();
    }


    setVisible(visible) {
        if(visible){
            this.loadDataList().then(()=>{
                this.setState({modalExportVisible: visible});
            })
        } else {
            this.setState({modalExportVisible: visible});
        }
    }

    async loadDataList(){
      let indexFiles = 0;
      let dataFiles = [];
      dataFiles.push({ key: indexFiles++, section: true, label: 'Descargas'});
      let files = await RNFetchBlob.fs.ls(RNFetchBlob.fs.dirs.DownloadDir);
      files.forEach(async (file) => {
        let isDir = await RNFetchBlob.fs.isDir(RNFetchBlob.fs.dirs.SDCardDir + '/' + file);
        if(!isDir && file.endsWith('.xlsx')){
          dataFiles.push({ key: indexFiles++, label: file });
        }
      });
      this.setState({
        dataFiles
      });
    }

    loadFile() {
        this.setVisible(false);
        this.props.navigator.showLoader();
        const fileName = RNFetchBlob.fs.dirs.DownloadDir + '/' + this.state.importFile;

        readFile(fileName, 'ascii').then(res => {
            const workbook = XLSX.read(res, {type:'buffer'});
            const firstSheetName = workbook.SheetNames[0];
            const worksheet = workbook.Sheets[firstSheetName];
            /* Get the value */
            const data = XLSX.utils.sheet_to_json(worksheet, this.props.header);
            this.props.onDataImport(data);
        }).catch(err => { Alert.alert("readFile Error", "Error " + err.message);
        }).finally(() => {this.props.navigator.hideLoader();});
    }

    render() {
        var modalBackgroundStyle = {
          backgroundColor: this.state.transparent ? 'rgba(0, 0, 0, 0.5)' : '#f5fcff',
        };
        var innerContainerTransparentStyle = this.state.transparent
          ? {backgroundColor: '#fff', padding: 20}
          : null;

        return <Modal
                animationType='none'
                transparent={this.state.transparent}
                visible={this.state.modalExportVisible}
                onRequestClose={() => this.setVisible(false)}
                supportedOrientations={['portrait', 'landscape']}
                onOrientationChange={evt => this.setState({currentOrientation: evt.nativeEvent.orientation})}
                >
                <View style={[styles.modalContainer, modalBackgroundStyle]}>
                  <View style={[styles.innerContainer, innerContainerTransparentStyle]}>
                    <View style={{alignItems:'center'}}>
                        <Text style={{marginBottom: 10, color: 'black', fontSize: 17}}>Importar productos</Text>
                        <ModalPicker
                            data={this.state.dataFiles}
                            initValue='Seleccione xlsx'
                            onChange={(option)=>{
                                this.setState({
                                  importFile: option.label
                                });
                            }} selectStyle={{flex:0, borderWidth:0}} selectTextStyle={{color:'#1565C0', marginBottom: 10}}>
                        </ModalPicker>
                        <Button onPress={this.loadFile.bind(this)} disabled={this.state.importFile == ''}
                            value={publishIcon} height={30} width={80} />
                     </View>
                  </View>
                </View>
            </Modal>
    }

}


const styles = StyleSheet.create({
    modalContainer: {
      flex: 1,
      justifyContent: 'center',
      padding: 20,
    },
    innerContainer: {
      borderRadius: 10,
      alignItems: 'center',
    }
})

export default DataImport;
