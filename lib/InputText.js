'use strict'

import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

class InputText extends Component {

    static defaultProps = {
        type: 'text',
        onChangeValue: () => { },
        onSubmitEditing: () => { },
        numberOfLines: 2,
        height: 35,
        required: false,
        editable: true,
        labelStyle: { },
        underlineColorAndroid: 'transparent',
        emailAddressLabel: 'mail@example.com',
        precision: 2,
        secureTextEntry: false,        
        autoCapitalize: 'sentences',
        iconStyle: {
            fontSize: 40,
            color: 'blue'
        },
        renderLabel: true
    }

    constructor(props) {
        super(props);
        this.inputText = null;
        let placeholder = props.placeholder ? props.placeholder : (props.type == 'email' ? this.props.emailAddressLabel : '');
        this.state = { value: '', placeholder, viewPassword: false, renderLabel: props.renderLabel };
        this.setTypeHandler(props.type);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (this.props.type != nextProps.type) {
            this.setTypeHandler(nextProps.type);
        }
        if(this.props.value != this.state.value){
            this.setState({value: this.props.value});
        }
    }

    setTypeHandler(type) {
        switch (type) {
            case 'decimal':
                this.onChangeText = this.onChangeDecimal.bind(this);
                break;
            case 'numeric':
                this.onChangeText = this.onChangeNumeric.bind(this);
                break;
            default:
                this.onChangeText = this.props.onChangeValue.bind(this);
        }        
        this.regex = this.props.precision == 2 ? /^(?:\d*\.\d{1,2}|\d*?|\d*\.?)$/ : /^(?:\d*\.\d{1,4}|\d*?|\d*\.?)$/;        
    }

    focus() {
        this.inputText.focus();
    }

    tryParseFloat(value) {
        return value.toString().match(this.regex) != null;
    }

    tryParseInt(value) {
        return value.toString().match(/^\d*$/) != null;
    }

    onChangeDecimal(value) {
        if (this.tryParseFloat(value)) {
            if (value.length > 1 && (value.startsWith('.') || value.startsWith('00'))) {
                value = parseFloat(value) + '';
            }
            this.props.onChangeValue(value);
        }
    }

    onChangeNumeric(value) {
        if (this.tryParseInt(value)) {
            this.props.onChangeValue(value);
        }
    }

    toogleViewPassword(){
        this.setState({viewPassword: !this.state.viewPassword});
    }

    render() {
        const readOnlyStyle = this.props.editable ? { color: 'black' } : styles.readOnlyStyle;
        let { value, viewPassword, placeholder, renderLabel } = this.state;
        return <View style={[styles.container, this.props.containerStyle]}>
            {(this.props.label && renderLabel) &&
                <View style={[{flexDirection: 'row', flex: 0.5}, this.props.labelStyle.flex]}>
                    <Text style={[styles.label, this.props.labelStyle]}>{this.props.label}</Text>
                    <Text style={styles.required}>{this.props.required && this.props.editable && '*'}</Text>
                </View>
            }
            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                <TextInput keyboardType={types[this.props.type]}
                    autoCapitalize={this.props.autoCapitalize}
                    underlineColorAndroid={this.props.underlineColorAndroid}
                    maxLength={this.props.maxLength}
                    editable={this.props.editable}
                    placeholder={renderLabel ? '' : placeholder}
                    onFocus={()=>{this.setState({renderLabel: true});}}
                    onBlur={()=>{this.setState({renderLabel: (value && value.length > 0) || this.props.renderLabel});}}
                    autoFocus={this.props.autoFocus}
                    autoCorrect={false}
                    style={[{ height: this.props.multiline ? 70 : this.props.height, borderBottomWidth:1, borderBottomColor: 'gray' }, styles.input, this.props.style, readOnlyStyle]}
                    onChangeText={value => { this.onChangeText(value); this.setState({value});}}
                    value={this.props.value ? this.props.value.toString() : ''}
                    multiline={this.props.multiline}
                    numberOfLines={this.props.numberOfLines}
                    secureTextEntry={(!viewPassword && this.props.secureTextEntry)}
                    ref={(input) => { this.inputText = input; }}
                    onSubmitEditing={this.props.onSubmitEditing}
                />
                {(!viewPassword && this.props.secureTextEntry) &&
                    <TouchableOpacity onPress={this.toogleViewPassword.bind(this)} style={{paddingVertical:10, paddingLeft:10}}>
                        <Text><Icon name="eye-off-outline" size={this.props.iconStyle.fontSize} color={this.props.iconStyle.color} /></Text>
                    </TouchableOpacity>
                }
                {(viewPassword && this.props.secureTextEntry) &&
                    <TouchableOpacity onPress={this.toogleViewPassword.bind(this)} style={{paddingVertical: 10, paddingLeft:10}}>
                        <Text><Icon name="eye-outline" size={this.props.iconStyle.fontSize} color={this.props.iconStyle.color} /></Text>
                    </TouchableOpacity>
                }                
            </View>
        </View>
    }

}

const types = {
    text: 'default',
    numeric: 'numeric',
    decimal: 'numeric',
    email: 'email-address',
    phone: 'phone-pad'
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        borderBottomColor: '#c8c7cc',
        borderBottomWidth: 0,        
        marginTop: 1,
        marginBottom: 1,
        minHeight:62,
        flexDirection: 'column', 
        width: '100%', 
        paddingHorizontal: 15
    },
    label: {        
        padding: 2,
        paddingBottom:0,
        color: '#696060', 
        fontWeight: '300',
        textAlignVertical: 'center'
    },
    input: {
        flex: 1,
        padding: 2
    },
    readOnlyStyle: {
        color: '#808080'
    },
    required: {
        color: 'red',
        textAlignVertical: 'center',
        paddingBottom: 4
    }
})

export default InputText;