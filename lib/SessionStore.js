import AsyncStorage from '@react-native-community/async-storage';

// AsyncStorage Proxy
var SessionStore = {
    set: (id, value) => {
        if (typeof value === 'object'){
            value = JSON.stringify(value);
        }
        AsyncStorage.setItem(id, value);
    },
    get: async (id) => {
        const value = await AsyncStorage.getItem(id);
        try {
          return JSON.parse(value);
        } catch (e) {
          return value;
        }
    },
    remove: (id) => {
        AsyncStorage.removeItem(id);
    },
    clear: () => {
        AsyncStorage.clear();
    }
};

export default SessionStore;
