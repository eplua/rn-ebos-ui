'use strict'

import React, {Component} from 'react';
import {DatePickerAndroid, DatePickerIOS, Platform ,TimePickerAndroid, StyleSheet, Text, TextInput, View} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Button from './Button';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Log from 'rn-ebos-ui/logger';

class InputDate extends Component {

    constructor(props) {
        super(props);
        let dateTime = props.dateTime ? new Date(props.dateTime) : new Date();        
        
        this.state = {
            dateTime,
            presetDate: dateTime,
            simpleDate: dateTime,
            spinnerDate: dateTime,
            calendarDate: dateTime,
            defaultDate: dateTime,
            allDate: dateTime,
            presetHour: dateTime.getHours(),
            presetMinute: dateTime.getMinutes(),
            mode: 'date',
            show: false
        };
    }

    static defaultProps = {
        onChangeValue: () => {},
        height: 35,
        required: false,
        editable: true,
        labelStyle: {flex: 0.5},
        maxDate: new Date(),
        minDate: null,
        showTime: false,
        style: {
            color: 'blue'
        },
        renderClose: true
    }

    state = {
        isoFormatText: 'pick a time (24-hour format)',
        simpleText: 'pick a datetime',
        spinnerText: 'pick a date',
        calendarText: 'pick a date',
        defaultText: 'pick a date',
        minText: 'pick a date, no earlier than today',
        maxText: 'pick a date, no later than today',
        presetText: 'pick a datetime',
        allText: 'pick a date between'
    };

    onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        this.setState({show: Platform.OS === 'ios', dateTime: currentDate});
        if(Platform.OS !== 'ios') {
            this.props.onChangeValue(currentDate);
        }
    }

    showMode = currentMode => {
        this.setState({show: true, mode: currentMode});
    }

    showDatePicker = () => {
        this.showMode('date');
    }

    showTimePicker = () => {
        this.showMode('time');
    }

    onDone = () => {
        this.setState({show: false});
        this.props.onChangeValue(this.state.dateTime);
    }

    onCancel = () => {
        this.setState({show: false});
    }

    showDatePickerdep = async(stateKey, options) => {
        try {
            var newState = {};
            const {action, year, month, day} = await DatePickerAndroid.open(options);
            if (action === DatePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            } else {
                let date = new Date(this.state.dateTime.getTime());
                date.setFullYear(year, month, day);
                newState[stateKey + 'Text'] = date.toLocaleDateString();
                newState[stateKey + 'Date'] = date;
                this.props.onChangeValue(date);
            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };

    showTimePicker = () => {
        let { presetDate } = this.state;
        let presetConf = {}; 
        if(this.props.maxDate){
            presetConf = { date: presetDate, maxDate: this.props.maxDate };
        } else if(this.props.minDate) {
            presetConf = { date: presetDate, minDate: this.props.minDate };
        } else {
            presetConf = { date: presetDate};
        }
        this._showTimePicker('preset', presetConf);   
    }

    _showTimePicker = async(stateKey, options) => {
        try {
            const {action, minute, hour} = await TimePickerAndroid.open(options);
            var newState = {};
            if (action === TimePickerAndroid.timeSetAction) {
                newState[stateKey + 'Text'] = hour + ':' + (minute < 10 ? '0' + minute : minute);
                newState[stateKey + 'Hour'] = hour;
                newState[stateKey + 'Minute'] = minute;
                let dateTime = new Date(this.state.dateTime.getTime());
                dateTime.setHours(hour);
                dateTime.setMinutes(minute);
                this.props.onChangeValue(dateTime);
            } else if (action === TimePickerAndroid.dismissedAction) {
                newState[stateKey + 'Text'] = 'dismissed';
            }
            this.setState(newState);
        } catch ({code, message}) {
            console.warn(`Error in example '${stateKey}': `, message);
        }
    };

    componentWillReceiveProps(props){
        if(this.props.datetime != props.dateTime){            
            this.setState({dateTime: new Date(props.dateTime)});
        }
    }

    render() {
        //const readOnlyStyle = this.props.editable ? {color:'black'} : styles.readOnlyStyle;
        let { presetDate, dateTime, presetHour, presetMinute, mode, show, renderClose } = this.state;    
        let presetConf = {}; 
        if(this.props.maxDate){
            presetConf = { date: presetDate, maxDate: this.props.maxDate };
        } else if(this.props.minDate) {
            presetConf = { date: presetDate, minDate: this.props.minDate };
        } else {
            presetConf = { date: presetDate};
        }
        return <View style={[styles.container, show ? {} : this.props.containerStyle]}>
            <View style={[{flexDirection: 'row', flex: 0.5}, this.props.labelStyle]}>
                <Text style={styles.label}>{this.props.label}</Text>
                <Text style={styles.required}>{this.props.required && this.props.editable && '*'}</Text>
            </View>
            {show && 
            <View>
                <DateTimePicker
                    value={dateTime}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    maximumDate={this.props.maxDate}
                    minimumDate={this.props.minDate}
                    onChange={this.onChange}
                />
                <View style={[{flexDirection:'row', flex:1, alignSelf: 'center'}, this.props.outputContainterStyle]}>
                    <Button onPress={this.onDone}
                        value={<Icon name="done" size={22} color={this.props.style.color}/>}                         
                        />
                    {this.props.renderClose &&
                    <Button onPress={this.onCancel}
                        value={<Icon name="close" size={22} color={this.props.style.color}/>}                  
                    />}
                </View>
            </View>}
            {!show && 
            <View style={[{flexDirection:'row', flex:1, alignItems: 'center'}, this.props.outputContainterStyle]}>
                <Text style={[styles.output, this.props.outputStyle]}>
                  { this.props.dateTime ? new Date(this.props.dateTime).toLocaleDateString() : dateTime.toLocaleDateString() }
                </Text>
                {this.props.editable &&
                    <Button onPress={this.showDatePicker.bind(this, 'preset', presetConf)}
                        value={<Icon name="today" size={18} color={this.props.style.color}/>}                         
                    />
                }
                {this.props.editable && this.props.showTime &&
                    <Button onPress={this._showTimePicker.bind(this, 'preset', {
                      hour: presetHour,
                      minute: presetMinute,
                    })}
                        value={<Icon name="access-time" size={18} color={this.props.style.color}/>}                         
                    />
                }
            </View>
            }
        </View>
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        borderBottomColor: '#c8c7cc',
        borderBottomWidth: 0,        
        marginTop: 1,
        marginBottom: 1,
        minHeight:62,
        flexDirection: 'column', 
        width: '100%', 
        paddingHorizontal: 15
    },
    label: {
        padding: 2,
        color: '#696060', 
        fontWeight: '300',
        textAlignVertical: 'center'
    },
    output: {
        //flex: 1,
        color: 'black',
        padding: 2,
    },
    readOnlyStyle: {
        color: '#808080'
    },
    required: {
        color:'red',
        textAlignVertical: 'center',
        paddingBottom: 4
    }
})

export default InputDate;
