'use strict'

import React, {Component} from 'react';
import {Modal, StyleSheet, View} from 'react-native';

class EbosModal extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        transparent: true,
        visible: false
    }

    render() {
        var modalBackgroundStyle = {
          backgroundColor: this.props.transparent ? 'rgba(0, 0, 0, 0.5)' : '#f5fcff',
        };
        var innerContainerTransparentStyle = this.props.transparent
          ? {backgroundColor: '#fff', padding: 20}
          : null;

        return <Modal
            animationType='none'
            transparent={this.props.transparent}
            visible={this.props.visible}
            onRequestClose={this.props.onRequestClose}
            supportedOrientations={['portrait', 'landscape']}
            onOrientationChange={evt => this.setState({currentOrientation: evt.nativeEvent.orientation})}
            >
            <View style={[styles.modalContainer, modalBackgroundStyle]}>
              <View style={[styles.innerContainer, innerContainerTransparentStyle]}>
                  {this.props.children}
              </View>
            </View>
        </Modal>
    }

}

const types = {
    text: 'default',
    decimal: 'numeric'
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        padding: 20,
    },
        innerContainer: {
        borderRadius: 10,
        alignItems: 'center',
    }
})

export default EbosModal;
