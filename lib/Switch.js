'use strict'

import React, {Component} from 'react';
import {StyleSheet, Text, View, Switch} from 'react-native';

class EbosSwitch extends Component {

    constructor(props) {
        super(props);
    }

    static defaultProps = {
        onChangeValue: () => {},
        editable: true
    }

    render() {
        return <View style={[styles.container, this.props.containerStyle]}>
            <Text style={[styles.label, this.props.labelStyle]}>{this.props.label}</Text>
            <View style={{flex:1, flexDirection:'row'}}>
                {this.props.leftDescription &&
                    <Text style={{textAlignVertical:'center', paddingRight:2}}>{this.props.leftDescription}</Text>
                }
                <Switch
                  style={this.props.style}
                  onValueChange={this.props.onChangeValue}
                  value={this.props.value}
                  disabled={!this.props.editable}
                />
                <Text style={styles.description}>{this.props.descripcion}</Text>
            </View>
        </View>
    }

}

const types = {
    text: 'default',
    decimal: 'numeric'
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      borderBottomColor: '#c8c7cc',
      borderBottomWidth: 0.5,
      flexDirection: 'row',
      marginBottom:1,
      marginTop:0.5
    },
    label: {
      flex: 1,
      padding: 2,
      color: 'black',
      textAlignVertical:'center'
    },
    description: {
      flex: 1,
      padding: 2,
      textAlignVertical:'center'
   }
})

export default EbosSwitch;
