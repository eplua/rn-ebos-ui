'use strict'

import React, { Component } from 'react';
import { Alert, Modal, StyleSheet, Text, View } from 'react-native';
import Button from './Button';
import Share from 'react-native-share';
import XLSX from 'xlsx';
import ModalPicker from 'react-native-modal-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import RNFetchBlob from 'rn-fetch-blob';

const saveIcon = (<Icon name="save" size={22} />)
const shareIcon = (<Icon name="share" size={22} color='gray' />);
const { writeFile, readFile, dirs: { DocumentDir } } = RNFetchBlob.fs;
const output = str => str.split("").map(x => x.charCodeAt(0));

class DataExport extends Component {

  static defaultProps = {
    saveLabel: 'Save',
    shareLabel: 'Share'
  }

  constructor(props) {
    super(props);
    this.state = {
      transparent: true,
      modalFilterVisible: false,
      currentOrientation: 'unknown',
      modalExportVisible: false,
      dataFiles: [],
      dataDirs: []
    };
  }

  componentDidMount() {
    this.loadDirList();
  }

  setWriteOpts(writeOpts) {
    this.setState({ writeOpts });
  }
  setVisible(visible) {
    this.setState({ modalExportVisible: visible });
  }

  xlsxWrite(writeOpts) {
    const data = this.props.dataLoad();
    if (!data || (data && data.length == 0)) {
      this.props.navigator.hideLoader();
      Alert.alert("Exportar", "No existen registros");
      return null;
    }
    const wb = XLSX.utils.book_new();

    if (data.sheets && data.sheets.length > 0) {
      data.sheets.forEach(sheet => {
        let ws = XLSX.utils.aoa_to_sheet([]);
        sheet.data.forEach(elem => {
          XLSX.utils.sheet_add_aoa(ws, elem[0], elem[1]);
        });
        XLSX.utils.book_append_sheet(wb, ws, sheet.name);
      });
    } else {
      let ws = XLSX.utils.json_to_sheet(data);
      XLSX.utils.book_append_sheet(wb, ws, writeOpts.filename);
    }
    /* write file */
    const wbout = XLSX.write(wb, writeOpts);
    return wbout;
  }

  async loadDirList() {
    try {
      let indexDirs = 0;
      let dataDirs = [];

      dataDirs.push({ key: indexDirs++, section: true, label: 'Tarjeta SD' });
      let files = await RNFetchBlob.fs.ls(RNFetchBlob.fs.dirs.DownloadDir);
      files = await RNFetchBlob.fs.ls(RNFetchBlob.fs.dirs.SDCardDir);
      files.forEach(async (file) => {
        let isDir = await RNFetchBlob.fs.isDir(RNFetchBlob.fs.dirs.SDCardDir + '/' + file);
        if (isDir) {
          dataDirs.push({ key: indexDirs++, label: file });
        }
      });
      this.setState({
        dataDirs
      });
    } catch (err) {
      console.log(err);
    }
  }

  saveFile(folder) {
    this.setVisible(false);
    this.props.navigator.showLoader();
    let writeOpts = this.state.writeOpts;
    writeOpts.type = 'binary';
    const wbout = this.xlsxWrite(writeOpts);
    if (!wbout) return;
    const path = RNFetchBlob.fs.dirs.SDCardDir + '/' + folder + "/";
    const file = path + writeOpts.filename + '.' + writeOpts.bookType;
    writeFile(file, output(wbout), 'ascii').then((res) => {
      Alert.alert("Guardado satisfactoriamente", "Guardado en " + file);
    }).catch(err => {
      Alert.alert("saveFile Error", "Error " + err.message);
    }).finally(() => { this.props.navigator.hideLoader(); });
  }

  shareFile() {
    this.setVisible(false);
    this.props.navigator.showLoader();
    let writeOpts = this.state.writeOpts;
    writeOpts.type = 'base64';
    const wbout = this.xlsxWrite(writeOpts);
    if (!wbout) return;
    let base64URLPrefix = "";
    switch (writeOpts.bookType) {
      case 'xlsx':
        base64URLPrefix = "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,";
        break;
      case 'csv':
        base64URLPrefix = "data:text/comma-separated-values;base64,";
        break;
    }
    const base64URL = base64URLPrefix + wbout;

    let shareImageBase64 = {
      title: writeOpts.title,
      message: writeOpts.message,
      url: base64URL,
      subject: writeOpts.subject //  for email
    };

    Share.open(shareImageBase64).catch((err) => {
      err && console.log(err);
    }).finally(() => { this.props.navigator.hideLoader(); });
  }

  render() {
    var modalBackgroundStyle = {
      backgroundColor: this.state.transparent ? 'rgba(0, 0, 0, 0.5)' : '#f5fcff',
    };
    var innerContainerTransparentStyle = this.state.transparent
      ? { backgroundColor: '#fff', padding: 20 }
      : null;

    return <Modal
      animationType='none'
      transparent={this.state.transparent}
      visible={this.state.modalExportVisible}
      onRequestClose={() => this.setVisible(false)}
      supportedOrientations={['portrait', 'landscape']}
      onOrientationChange={evt => this.setState({ currentOrientation: evt.nativeEvent.orientation })}
    >
      <View style={[styles.modalContainer, modalBackgroundStyle]}>
        <View style={[styles.innerContainer, innerContainerTransparentStyle]}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
              <Text style={{ flex: 1 }}>{this.props.saveLabel}</Text>
              <ModalPicker
                style={{ flex: 1 }}
                data={this.state.dataDirs}
                initValue='Select something yummy!'
                onChange={(option) => {
                  this.saveFile(option.label);
                }}
              >
                <Text>{saveIcon}</Text>
              </ModalPicker>
            </View>
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
              <Text>{this.props.shareLabel}</Text>
              <Button onPress={() => {
                this.shareFile();
              }}
                value={shareIcon} color='transparent'
              />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  }

}


const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  innerContainer: {
    borderRadius: 10,
    alignItems: 'center',
  }
})

export default DataExport;
