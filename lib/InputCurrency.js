'use strict'

import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

class InputCurrency extends Component {

    constructor(props) {
        super(props);
        if(props.forceParse2){
            this.tryParseFloat = this.tryParseFloat2.bind(this);
        } else {
            this.tryParseFloat = System.MONEDA_DECIMALES == 2 ? this.tryParseFloat2.bind(this) : this.tryParseFloat4.bind(this);
        }
    }

    static defaultProps = {
        onChangeValue: () => {},
        required: false,
        editable: true,
        labelStyle: {flex:0.5},
        outputStyle: {flex:1},
        forceParse2: false
    };

    tryParseFloat2(value) {
      if (value.toString().match(/^(?:\d*\.\d{1,2}|\d*?|\d*\.?)$/) != null) {
        return true;
      }
      else return false;
    }

    tryParseFloat4(value) {
      if (value.toString().match(/^(?:\d*\.\d{1,4}|\d*?|\d*\.?)$/) != null) {
        return true;
      }
      else return false;
    }

    onChangeText(value){
        if(this.tryParseFloat(value)){
            if(value.length > 1 && value.startsWith('.')){
                value = parseFloat(value);
            }
            this.props.onChangeValue(value);
         }
    }

    render() {
        const readOnlyStyle = this.props.editable ? {color:'black'} : styles.readOnlyStyle;
        return <View style={styles.container}>
            {this.props.label &&
                <View style={{flexDirection: 'row', flex:this.props.labelStyle.flex}}>
                    <Text style={[styles.label, this.props.style]}>{this.props.label}</Text>
                    <Text style={styles.required}>{this.props.required && this.props.editable && '*'}</Text>
                </View>
            }
            <View style={{flexDirection: 'row', flex:this.props.outputStyle.flex}}>
                <Text style={[{textAlignVertical:'center', color: 'black', paddingLeft:3,paddingRight:2}, readOnlyStyle, this.props.style]}>
                    {this.props.currencySymbol ? this.props.currencySymbol : System.SIMBOLO_MONEDA}
                </Text>
                <TextInput keyboardType='numeric'
                    editable={this.props.editable}
                    autoFocus={this.props.autoFocus}
                    autoCorrect={false} style={[{
                        padding:2.5,
                        flex: 1
                    }, readOnlyStyle, this.props.style]}
                    onChangeText={this.onChangeText.bind(this)}
                    value={this.props.value? this.props.value.toString() : ''}/>
            </View>
        </View>
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        borderBottomColor: '#c8c7cc',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        marginTop:0.75,
        marginBottom:0.75
    },
    label: {
        padding: 2,
        color: 'black',
        textAlignVertical:'center'
    },
    readOnlyStyle: {
        color: '#808080'
    },
    required: {
        color:'red',
        textAlignVertical: 'center',
        paddingBottom: 4
    }
})

export default InputCurrency;
