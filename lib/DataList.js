'use strict'

import React, {Component} from 'react';
import {FlatList, RefreshControl, StyleSheet, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Button from './Button';
import InputText from './InputText';
import Config from 'react-native-config';
import Icon from 'react-native-vector-icons/MaterialIcons';
const nextIcon = (<Icon name="navigate-next" size={20} color='gray'/>);
const beforeIcon = (<Icon name="navigate-before" size={20} color='gray'/>);

class DataList extends Component {

    constructor(props) {
        super(props);
        this.MAX_ROW_PAGE = Number(Config.MAX_ROW_PAGE);
        this.defaultPagination = {
            maxRowPage: this.MAX_ROW_PAGE,
            initRow: 0,
            endRow: this.MAX_ROW_PAGE,
            sort: props.sort,
            size: 0,
            pageSize: 0
        };

        const renderHeader = props.renderHeader ? props.renderHeader(this.sortList.bind(this)) : null;

        this.state = {
            query: '',
            pagination: this.defaultPagination,
            renderHeader,
            dataSource: [],
            refreshing: false
        };
    }

    static defaultProps = {
        sort: ['id', false],
        renderSearch: true,
        searchLabel: 'Search'
    };

    componentDidMount() {
        this.searchVal('');
    }

    isPromise(value) {
        return Boolean(value && typeof value.then === 'function');
    }

    getSize(){
        return this.state.pagination ? this.state.pagination.pageSize : 0;
    }

    pageNavigate(next){
        let pagination = {...this.state.pagination};
        const initRow = pagination.initRow, endRow = pagination.endRow, pageSize = pagination.maxRowPage;
        pagination.initRow = next ? initRow + pageSize : initRow - pageSize;
        pagination.endRow = next ? endRow + pageSize : endRow - pageSize;        
        const list0 = this.props.loadDataList(this.state.query, pagination);
        if(this.isPromise(list0)){
            list0.then(data => {
                let { list, pagination } = data;
                //console.log('List length:', list.length);
                pagination.pageSize = list.length;
                this.setState({
                    dataSource: list,
                    pagination
                });
            });
        } else {            
            pagination.pageSize = list0.length;
            this.setState({
                dataSource: list0,
                pagination
            });
        }
    }

    sortList(sortColumn){
        let pagination = {...this.defaultPagination};                
        const order = this.state.pagination.sort[0] == sortColumn ? !this.state.pagination.sort[1] : false;
        pagination.sort = [sortColumn, order];
        const list = this.props.loadDataList(this.state.query, pagination);
        if(this.isPromise(list)){
            list.then(data => {
                let { list, pagination } = data;
                pagination.pageSize = list.length;
                this.setState({
                    dataSource: list,
                    pagination
                });
            });
        } else {            
            pagination.pageSize = list.length;
            this.setState({
                dataSource: list,
                pagination
            });
        }
    }

    searchVal(query){
        if(!this.props.renderSearch) {
            let list0 = this.props.loadDataList();
            if(this.isPromise(list0)){
                list0.then(data => {
                    this.setState({
                        dataSource: data.list //this.state.dataSource.cloneWithRows(data.list)
                    });
                });
            } else {
                this.setState({
                  dataSource: list0
                });
            }
        } else {
            let pagination = this.defaultPagination;
            pagination.sort = this.state.pagination.sort;
            let list = this.props.loadDataList(query, pagination);
            if(this.isPromise(list)){
                this.setState({ query });
                list.then(data => {
                    let { list, pagination } = data;
                    pagination.pageSize = list.length;
                    this.setState({
                        dataSource: list,
                        pagination,
                        refreshing: false
                    });
                });
            } else {                
                pagination.pageSize = list.length;
                this.setState({
                  dataSource: list,
                  pagination,
                  query,
                  refreshing: false
                });
            }
        }
    }

    update(){
        this.searchVal('');
    }

    _onRefresh = () => {
        this.setState({refreshing: true});        
        this.searchVal(this.state.query);
    }

    reload(){
        let pagination = this.state.pagination;
        pagination.sort = this.props.sort;
        let list = this.props.loadDataList(this.state.query, pagination);        
        if(this.isPromise(list)){            
            list.then(data => {
                let { list, pagination } = data;
                pagination.pageSize = list.length;
                this.setState({
                    dataSource: list,
                    pagination
                });
            });
        } else {            
            pagination.pageSize = list.length;
            this.setState({            
                dataSource: list,
                pagination
            });
        }
    }

    render(){
        const { query, pagination } = this.state;
        return <View style={{flex:1}}>
            {this.props.renderSearch &&
              <View style={{flexDirection:'row', alignItems: 'center', height:40}}>
                <InputText maxLength={100} value={query} onChangeValue={this.searchVal.bind(this)}
                        style={{fontSize:16, borderBottomWidth: 0}} placeholder={this.props.searchLabel + '...'} renderLabel={false}
                        containerStyle={{flex:1}} underlineColorAndroid='transparent' autoCapitalize='none'/>
                {(query && query.length) > 0 &&
                    <TouchableOpacity style={{paddingHorizontal:15, paddingVertical:12}} onPress={()=>{
                        this.searchVal('');
                    }}>
                        <Text><Icon name="close" size={14}/></Text>
                    </TouchableOpacity>
                }
              </View>
            }
            {this.state.renderHeader}
                <FlatList
                    style={{flex:0.84}} contentContainerStyle={styles.container}
                    keyboardShouldPersistTaps='always' 
                    data = {this.state.dataSource}
                    renderItem = {this.props.renderRow}
                    refreshControl={
                        <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this._onRefresh}
                        />
                    }   
                />
            {pagination.size > 0 &&
            <View style={{flex:0.07, flexDirection:'row', alignItems: 'center', justifyContent:'center', marginRight:5, marginLeft:10}}>
                <Text style={{flex:1}}>Reg: {pagination.initRow + 1}-{pagination.initRow + pagination.pageSize}/{pagination.size}</Text>
                <Button onPress={() => {this.pageNavigate(false);}} width={65}
                  value={beforeIcon} disabled={pagination.initRow == 0} color='transparent'
                  containerStyle={{flex:0.75}}
                />
                <Button onPress={() => {this.pageNavigate(true);}} width={65} color='transparent'
                  value={nextIcon} disabled={(pagination.initRow + pagination.pageSize) == pagination.size}
                  containerStyle={{flex:0.75}}
                />
                <Text style={{flex:1}}>Pag: {Math.ceil(pagination.initRow / pagination.maxRowPage) + 1}/{Math.ceil(pagination.size/pagination.maxRowPage)}</Text>
            </View>
            }
        </View>
    }

}

const styles = StyleSheet.create({
   container: {
      backgroundColor: 'white',
      flexGrow: 1
    }
});

export default DataList;