'use strict'

import React, {Component} from 'react';
import {StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import ModalDropdown from './ModalDropdown';
import Button from './Button';
import Icon from 'react-native-vector-icons/MaterialIcons';
const dropDownIcon = (<Icon name="arrow-drop-down" size={20} color='gray'/>)

class InputSelect extends Component {

    constructor(props) {
        super(props);
        this.inputSelect = null;
        let objSelection = {};
        objSelection[this.props.objectKey] = -1,
        objSelection['toString'] = () => {
            return this.props.placeholder;
        };
        this.state = {
            objSelection
        };
    }

    static defaultProps = {
        type: 'text',
        onChangeValue: () => {},
        height: 35,
        placeholder: 'Select...',
        textStyle: {fontSize:14, color: 'black'},
        objectKey: 'id',
        editable: true,
        required: false
    }

    focus(){
        this.inputSelect.focus();
    }

    _renderRow(rowData, rowID, highlighted) {
        let evenRow = rowID % 2;
        return (<View style={[styles.row]}>
            <Text style={[styles.row_text, highlighted && {color: 'red'}]}>
            {rowData.toString()}
            </Text>
        </View>);
       /* return (
          <TouchableHighlight underlayColor='cornflowerblue'>
            <View style={[styles.row]}>
              <Text style={[styles.row_text, highlighted && {color: 'red'}]}>
                {rowData.toString()}
              </Text>
            </View>
          </TouchableHighlight>
        );*/
      }

    render() {
        const readOnlyStyle = this.props.editable ? {color:'black'} : styles.readOnlyStyle;
        const placeholderText = this.props.editable ? this.props.placeholder : '';
        const data = [...this.props.data];
        let selected = this.props.value;
        let index = -1;
        if(selected){
            if(!this.props.required){
                data.unshift(this.state.objSelection);
            }
            index = data.findIndex((elem) => {return elem[this.props.objectKey] == selected[this.props.objectKey]});
        }

        return <View style={[styles.container, this.props.containerStyle]}>
            {this.props.label &&
                <View style={[{flexDirection: 'row', flex:0.5}, this.props.labelStyle]}>
                    <Text style={styles.label}>{this.props.label}</Text>
                    <Text style={styles.required}>{this.props.required && this.props.editable && '*'}</Text>
                </View>
            }
            <View style={{flex:1, flexDirection: 'row'}}>
                <ModalDropdown style={[styles.input,this.props.style]}
                    disabled={!this.props.editable}
                    defaultIndex={index}
                    defaultValue={selected ? (selected.toString() ? selected.toString() : placeholderText) : placeholderText}
                    renderRow={this._renderRow.bind(this)}
                    textStyle={[this.props.textStyle, readOnlyStyle]}
                    options={data}
                    onSelect={(idx, entity) => {
                        entity = entity[this.props.objectKey] == -1 ? null : (this.props.objectKey == 'id'? entity : entity[this.props.objectKey]);
                        this.props.onSelect(entity);
                    }}
                    ref={(input) => {this.inputSelect = input;}}
                />
                {this.props.editable &&
                    <Button onPress={()=>{this.inputSelect._onButtonPress()}}
                      value={dropDownIcon} color='transparent' style={{width: 25}} height={this.props.height}
                    />
                }
            </View>
        </View>
    }

}

const types = {
    text: 'default',
    decimal: 'numeric'
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      borderBottomColor: '#c8c7cc',
      borderBottomWidth: 0.5,
      flexDirection: 'row',
      marginTop:1,
      marginBottom:1      
    },
    label: {
      padding: 2,
      color: 'black',
      textAlignVertical:'center'
    },
    input: {
      flex: 1,
      padding: 2,
      marginRight:-3,
      paddingRight:0
    },
    row: {
        flexDirection: 'row',
        height: 35,
        alignItems: 'center',
        paddingHorizontal:10

    },
    row_text: {
        marginHorizontal: 10,
        fontSize: 16,
        color: 'rgba(0, 0, 0, 0.87)',
        textAlignVertical: 'center',
    },
    readOnlyStyle: {
        color: '#808080'
    },
    required: {
        color:'red',
        textAlignVertical: 'center',
        paddingBottom: 4
    }
})

export default InputSelect;
