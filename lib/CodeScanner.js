'use strict';

import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import RNCamera from 'react-native-camera';
import Viewfinder from './Viewfinder';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  overlay: {
    position: 'absolute',
    padding: 16,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  topOverlay: {
    top: 0,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bottomOverlay: {
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  captureButton: {
    padding: 15,
    backgroundColor: 'white',
    borderRadius: 40,
  },
  typeButton: {
    padding: 5,
  },
  flashButton: {
    padding: 5,
  },
  buttonsSpace: {
    width: 10,
  },
});

export default class CodeScanner extends React.Component {
  constructor(props) {
    super(props);

    this.camera = null;

    this.state = {
      camera: {
        aspect: RNCamera.constants.Aspect.fill,
        captureTarget: RNCamera.constants.CaptureTarget.cameraRoll,
        type: RNCamera.constants.Type.back,
        orientation: RNCamera.constants.Orientation.auto,
        flashMode: RNCamera.constants.FlashMode.auto,
      },
      isRecording: false
    };

    console.log('Run camera...');
  }

  render() {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={(cam) => {
            this.camera = cam;
          }}
          style={styles.preview}
          aspect={this.state.camera.aspect}
          captureTarget={this.state.camera.captureTarget}
          type={this.state.camera.type}
          flashMode={this.state.camera.flashMode}
          defaultTouchToFocus
          mirrorImage={false}
          onBarCodeRead={this.props.onBarCodeRead}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}
        >
          <Viewfinder
            backgroundColor={this.props.viewFinderBackgroundColor}
            color={this.props.viewFinderBorderColor}
            borderWidth={this.props.viewFinderBorderWidth}
            borderLength={this.props.viewFinderBorderLength}
            height={this.props.viewFinderHeight}
            isLoading={this.props.viewFinderShowLoadingIndicator}
            width={this.props.viewFinderWidth}
          />
        </RNCamera>

      </View>
    );
  }
}
