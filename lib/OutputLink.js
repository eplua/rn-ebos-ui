'use strict'

import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

class OutputLink extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return <View style={[styles.container, this.props.containerStyle]}>
            <Text style={[styles.label, this.props.labelStyle, this.props.style]}>{this.props.label}</Text>
            {this.props.value? (
                <TouchableOpacity onPress={this.props.onPress} style={{flex:1}}>
                    <Text style={[styles.output, this.props.outputStyle, this.props.style]}>{this.props.value}</Text>
                </TouchableOpacity>) : (
                    <Text style={[styles.output, this.props.outputStyle, this.props.style]}></Text>
                )
            }
        </View>
    }

}

const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      backgroundColor: 'white',
      borderBottomColor: '#c8c7cc',
      borderBottomWidth: 0.5,
      marginTop:1,
      marginBottom:1
    },
    label: {
        flex: 0.5,
        padding: 2,
        color: 'black',
        textAlignVertical:'center'
    },
    output: {
        flex: 1,
        padding: 2,
        textAlignVertical:'center',
        color: '#4F8EF7'
    }
})

export default OutputLink;
