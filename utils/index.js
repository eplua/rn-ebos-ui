'use strict'

const formatDateSimple = (date) => {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    let today = new Date();
    today.setHours(0,0,0,0);

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

const formatDate = (date) => {
    if(!date){
        return '';
    }
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    let today = new Date();
    today.setHours(0,0,0,0);
    const tomorrow = new Date(today.getTime() + (24 * 60 * 60 * 1000));

    if(d > today && d < tomorrow){
        return '';
    }

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

const formatDateTime = (date) => {
     return formatDateSimple(date) + ' ' + date.toLocaleTimeString();
}

const getDiffDays = (from, to) => {
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    var diffDays = Math.round(Math.abs((from.getTime() - to.getTime()) / (oneDay)));
    return diffDays;
}

const formatDateTimeSimple = (date) => {
    const result = formatDate(date);
    return (result? result + ' ' : '') + date.getHours() + ':' + date.getMinutes()
}

const getFromToMonth = () => {
    let now = new Date(), y = now.getFullYear(), m = now.getMonth();
    let fechaDesde = new Date(y, m, 1), fechaHasta = new Date(y, m + 1, 0);
    fechaDesde.setHours(0,0,0,0);
    fechaHasta.setHours(23,59,59,0);
    return {fechaDesde, fechaHasta};
}

const DateUtils = {
    formatDate,
    formatDateSimple,
    formatDateTime,
    formatDateTimePlain: (date) => {
        let result = formatDateTime(date);
        return result.replace(/-|:/g,'').replace(/ /, '_');
    },
    getDiffDays,
    formatDateTimeSimple,
    getFromToMonth
}

export default DateUtils;
