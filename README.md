# Instalation


## Componentes

### Camera
```
react-native link react-native-camera
```
### Share
```
react-native link react-native-share
```

### Fetch Blob
RNFB_ANDROID_PERMISSIONS=true react-native link rn-fetch-blob

[react-native-camera](https://www.npmjs.com/package/react-native-camera)
[react-native-share](https://www.npmjs.com/package/react-native-share)
[rn-fetch-blob](https://github.com/joltup/rn-fetch-blob/tree/v0.10.15)