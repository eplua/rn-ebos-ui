'use strict'

import CryptoJS from 'crypto-js';
import SessionStore from '../lib/SessionStore';
import Config from 'react-native-config';

// Logging
import Log from '../logger';

const Method = {
  GET: 'GET',
  POST: 'POST'
}

export default class APIService {

    constructor(event, API_URL) {
        this.ee = event;
        this.API_URL = API_URL ? API_URL : Config.API_URL;
    }

    // API

    async _fetch({url, method, version, token, data, formData, encrypt, silent, hideError}){
        try{
            !silent && this.ee.emit('app.showLoader');
            let tokenHeader = null, bodyParam = null, contentType = 'application/json';
            if(!method){
                method = Method.GET;
            }
            if(!version){
                version = '0.1.0';
            }
            if(!token){
                token = await SessionStore.get('_0_jt');
                if(token){
                    tokenHeader = { 'x-access-token': token }
                }
            } else {
                tokenHeader = { 'x-access-token': token }
            }
            if(data){
                if(encrypt){
                    const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), token);
                    bodyParam = { body: JSON.stringify({_4_de: ciphertext.toString()}) };
                } else {
                    bodyParam = { body: JSON.stringify({_3_dt: data}) };
                }
            } else if(formData) {
                let data = new FormData();  
                data.append('_3_dt', formData);
                bodyParam = { body: data};
                contentType = 'multipart/form-data';
            }

            let status = 500;
            Log.trace('FETCH', url);
            return await fetch(url, {
                method: method,
                mode: 'cors',
                redirect: 'follow',
                ...bodyParam,
                headers: new Headers({
                    'accept': 'application/json',
                    'content-type': contentType,
                    'accept-version': version,
                    ...tokenHeader
                })
            }).then(res => {
                status = res.status;
                return res.text();
            }).then(text => text.length ? JSON.parse(text) : {}
            ).then(data => {
                if(status >= 400){
                    if(status == 401) {
                        this.logout();
                    }
                    Log.warn('FETCH[1]', {status, warn: data});
                    !hideError && this.ee.emit('app.handleFetchException', {status, warn: data});
                    return {status};
                } else if(status >= 500){
                    Log.error('FETCH[1]', {status, error: data});
                    !hideError && this.ee.emit('app.handleFetchException', {status, error: data});
                    return {status};
                }
                Log.trace('FETCH[1]', {status, data});
                return {status, data};
            }).catch(err => {                
                let errObj = { status };
                if(err.toString().includes('Network request failed') || status == 502){
                    errObj.error = 'api.error.serviceUnavailable';
                } else {
                    errObj.fatal = err.toString();
                }
                Log.error('FETCH[2]', errObj);
                !hideError && this.ee.emit('app.handleFetchException', errObj);
                return errObj;
            });            
        } catch(fatal) {
            Log.error('FETCH[3]', { fatal });
            !hideError && this.ee.emit('app.handleFetchException', { fatal });
            return { status: 500, fatal }
        } finally {
            !silent && this.ee.emit('app.hideLoader');
        }
    }

    // SECURITY
    async getConsentToken() {
        let resp = null;        
        Log.debug('Request consent...');
        await this._fetch({
            url: this.API_URL + '/security/consent',
            method: Method.POST,
            silent: true
        }).then(res => {
            resp = res;
        });
        if(resp.status == 200){
            return resp.data._0_jt;
        } else {
            return resp;
        }
        
    }

    async login(username, password, cb) {
        Log.info('Login', 'Authenticating...');        
        let consentToken = await this.getConsentToken();

        this._fetch({
          url: this.API_URL + '/security/authenticate',
          method: Method.POST,
          data: {username, password},
          encrypt: true,
          token: consentToken,
          silent: true
        }).then(async (res) => {            
            if(res.status == 200){
                SessionStore.set('_0_jt', res.data._0_jt);            
                Log.debug('Load account...');
                cb(null, await this.loadAccount());
            } else {
                cb(res);
            }
        });   
    }

    /**
     * 
     * @param {"id":"232551967859450","email":"user@mail.com","name":"First Name"} auth 
     */
    async loginFB(auth, cb) {
        Log.info('Login Facebook', 'Authenticating...');        
        let consentToken = await this.getConsentToken();

        this._fetch({
          url: this.API_URL + '/security/authenticate',
          method: Method.POST,
          data: { fb : auth },
          encrypt: true,
          token: consentToken,
          silent: true
        }).then(async (res) => {
            if(res.status == 200){
                SessionStore.set('_0_jt', res.data._0_jt);            
                Log.debug('Load account...');
                cb(null, await this.loadAccount());
            } else {
                cb(res);
            }
        });
    }

    /**
     * 
     * @param {"id":"232551967859450","email":"user@mail.com","name":"First Name"} auth 
     */
     async loginGG(auth, cb) {
        Log.info('Login Google', 'Authenticating...');        
        let consentToken = await this.getConsentToken();

        this._fetch({
          url: this.API_URL + '/security/authenticate',
          method: Method.POST,
          data: { gg : auth },
          encrypt: true,
          token: consentToken,
          silent: true
        }).then(async (res) => {
            if(res.status == 200){
                SessionStore.set('_0_jt', res.data._0_jt);            
                Log.debug('Load account...');
                cb(null, await this.loadAccount());
            } else {
                cb(res);
            }
        });
    }

    /**
     * {
     *  "code": "c64863743be3044deac2f42def279a02a.0.mrvqu.jJiCcAefmpXpSuMu4SmzTA", 
     *  "id_token": "eyJraWQiOiJlWGF1bm1MIiwiYWxnIjoiUlMyNTYifQ.eyJpc3MiOiJodHRwczovL2FwcGxla...", 
     *  "nonce": "0b982e79-f198-42ac-8bec-a607166e50d1", 
     *  "state": "c6ccb520-2ef8-4025-9e27-939ab36dbf89",
     *  "user": {
     *           "email": "eduardo.plua@gmail.com", 
     *           "name": { 
     *                    "firstName": "Eduardo", 
     *                    "lastName": "Plúa Alay"
     *                   }
     *          }
     * }
     */
     async loginApl(auth, cb) {
        console.log(auth);
        Log.info('Login Apple', 'Authenticating...');
        let consentToken = await this.getConsentToken();

        this._fetch({
          url: this.API_URL + '/security/authenticate',
          method: Method.POST,
          data: { apl : auth },
          encrypt: true,
          token: consentToken,
          silent: true
        }).then(async (res) => {
            if(res.status == 200){
                SessionStore.set('_0_jt', res.data._0_jt);            
                Log.debug('Load account...');
                cb(null, await this.loadAccount());
            } else {
                cb(res);
            }
        });
    }

    logout() {
        //this.API_PASSWORD = this.API_USER = null;
        SessionStore.clear();
    }

    /*async reconnection(){
        Log.debug('Reconnection')
        this.logout();
        this.login(this.API_USER, this.API_PASSWORD).then(res => {
            if(res.status == 200){                
                Log.info('Reconnection', 'Connection Successfully');
            } else {
                Log.info('Reconnection', 'Connection Failed', res.status);
            }
        });
    }*/

    async loadAccount() {
        let res = await this._fetch({
            url: this.API_URL + '/security/account',
            silent: true
        });
        if(res.status == 200){
            SessionStore.set('_2_ac', res.data);
        }
        return res.data
    }

    getAccount() {
        return SessionStore.get('_2_ac');
    }

    async isConnected() {
        let _2_ac = await SessionStore.get('_2_ac');
        return _2_ac != null;
    }

}